#ifndef AST_HPP
#define AST_HPP

#include <vector>
#include <cstdint>
#include <string>

class ASTNode{
public:
    enum class Type{
        Top,
        Integer,
        Boolean,
        Null,
        Variable,
        AccessVariable,
        AssignVariable,
        Array,
        AccessArray,
        AssignArray,
        Function,
        CallFunction,
        Print,
        Block,
        Loop,
        Conditional,
        Object,
        AssignField,
        AccessField,
        CallMethod
    };

    virtual ~ASTNode(void) = default;
    virtual Type getType(void) const = 0;
};
class ASTInteger: public ASTNode{
public:
    const int32_t value;

    ASTInteger(int32_t pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ASTBoolean: public ASTNode{
public:
    const bool value;

    ASTBoolean(bool pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ASTNull: public ASTNode{
public:
    ASTNull(void) = default;
    Type getType(void) const override;
};
class ASTVariable: public ASTNode{
public:
    const std::string name;
    const ASTNode *value;

    ASTVariable(const std::string& pname, ASTNode *pvalue)
    : name(pname), value(pvalue){
    }
    ~ASTVariable(void);
    Type getType(void) const override;
};
class ASTAccessVariable: public ASTNode{
public:
    const std::string name;

    ASTAccessVariable(const std::string& pname)
    : name(pname){
    }
    Type getType(void) const override;
};
class ASTAssignVariable: public ASTNode{
public:
    const std::string name;
    const ASTNode *value;

public:
    ASTAssignVariable(const std::string& pname, ASTNode *pvalue)
    : name(pname), value(pvalue){
    }
    ~ASTAssignVariable(void);
    Type getType(void) const override;
};
class ASTArray: public ASTNode{
public:
    const ASTNode *size;
    const ASTNode *value;

public:
    ASTArray(ASTNode *psize, ASTNode *pvalue)
    : size(psize), value(pvalue){
    }
    ~ASTArray(void);
    Type getType(void) const override;
};
class ASTAccessArray: public ASTNode{
public:
    const ASTNode *array;
    const ASTNode *index;

public:
    ASTAccessArray(ASTNode *parray, ASTNode *pindex)
    : array(parray), index(pindex){
    }
    ~ASTAccessArray(void);
    Type getType(void) const override;
};
class ASTAssignArray: public ASTNode{
public:
    const ASTNode *array;
    const ASTNode *index;
    const ASTNode *value;

public:
    ASTAssignArray(ASTNode *parray, ASTNode *pindex, ASTNode *pvalue)
    : array(parray), index(pindex), value(pvalue){
    }
    ~ASTAssignArray(void);
    Type getType(void) const override;
};
class ASTFunction: public ASTNode{
public:
    const std::string name;
    const std::vector<std::string> parameters;
    const ASTNode *body;

    ASTFunction(const std::string& pname, const std::vector<std::string>& pparameters, ASTNode *pbody)
    : name(pname), parameters(pparameters), body(pbody){
    }
    ~ASTFunction(void);
    Type getType(void) const override;
};
class ASTCallFunction: public ASTNode{
public:
    const std::string name;
    const std::vector<ASTNode *> arguments;

    ASTCallFunction(const std::string& pname, const std::vector<ASTNode *>& parguments)
    : name(pname), arguments(parguments){
    }
    ~ASTCallFunction(void);
    Type getType(void) const override;
};
class ASTPrint: public ASTNode{
public:
    const std::string format;
    const std::vector<ASTNode *> arguments;

    ASTPrint(const std::string& pformat, const std::vector<ASTNode *>& parguments)
    : format(pformat), arguments(parguments){
    }
    ~ASTPrint(void);
    Type getType(void) const override;
};
class ASTBlock: public ASTNode{
public:
    const std::vector<ASTNode *> children;

    ASTBlock(const std::vector<ASTNode *>& pchildren)
    : children(pchildren){
    }
    ~ASTBlock(void);
    Type getType(void) const override;
};
class ASTTop: public ASTNode{
public:
    const std::vector<ASTNode *> children;

public:
    ASTTop(const std::vector<ASTNode *>& pchildren)
    : children(pchildren){
    }
    ~ASTTop(void);
    Type getType(void) const override;
};
class ASTLoop: public ASTNode{
public:
    const ASTNode *condition;
    const ASTNode *body;

    ASTLoop(ASTNode *pcondition, ASTNode *pbody)
    : condition(pcondition), body(pbody){
    }
    ~ASTLoop(void);
    Type getType(void) const override;
};
class ASTConditional: public ASTNode{
public:
    const ASTNode *condition;
    const ASTNode *consequent;
    const ASTNode *alternative;

    ASTConditional(ASTNode *pcondition, ASTNode *pconsequent, ASTNode *palternative)
    : condition(pcondition), consequent(pconsequent), alternative(palternative){
    }
    ~ASTConditional(void);
    Type getType(void) const override;
};
class ASTObject: public ASTNode{
public:
    const ASTNode *extends;
    const std::vector<ASTNode *> members;

    ASTObject(ASTNode *pextends, const std::vector<ASTNode *> pmembers)
    : extends(pextends), members(pmembers){
    }
    ~ASTObject(void);
    Type getType(void) const override;
};
class ASTAssignField: public ASTNode{
public:
    const ASTNode *object;
    const std::string field;
    const ASTNode *value;

    ASTAssignField(ASTNode *pobject, const std::string& pfield, ASTNode *pvalue)
    : object(pobject), field(pfield), value(pvalue){
    }
    ~ASTAssignField(void);
    Type getType(void) const override;
};
class ASTAccessField: public ASTNode{
public:
    const ASTNode *object;
    const std::string field;

    ASTAccessField(ASTNode *pobject, const std::string& pfield)
    : object(pobject), field(pfield){
    }
    ~ASTAccessField(void);
    Type getType(void) const override;
};
class ASTCallMethod: public ASTNode{
public:
    const ASTNode *object;
    const std::string name;
    const std::vector<ASTNode *> arguments;

    ASTCallMethod(ASTNode *pobject, const std::string& pname, const std::vector<ASTNode *>& parguments)
    : object(pobject), name(pname), arguments(parguments){
    }
    ~ASTCallMethod(void);
    Type getType(void) const override;
};

#endif
