#include "ast.hpp"

#include <vector>
#include <cstdint>
#include <string>

ASTNode::Type ASTInteger::getType(void) const{
    return Type::Integer;
}

ASTNode::Type ASTBoolean::getType(void) const{
    return Type::Boolean;
}

ASTNode::Type ASTNull::getType(void) const{
    return Type::Null;
}

ASTVariable::~ASTVariable(void){
    delete value;
}
ASTNode::Type ASTVariable::getType(void) const{
    return Type::Variable;
}

ASTNode::Type ASTAccessVariable::getType(void) const{
    return Type::AccessVariable;
}

ASTAssignVariable::~ASTAssignVariable(void){
    delete value;
}
ASTNode::Type ASTAssignVariable::getType(void) const{
    return Type::AssignVariable;
}

ASTArray::~ASTArray(void){
    delete size;
    delete value;
}
ASTNode::Type ASTArray::getType(void) const{
    return Type::Array;
}

ASTAccessArray::~ASTAccessArray(void){
    delete array;
    delete index;
}
ASTNode::Type ASTAccessArray::getType(void) const{
    return Type::AccessArray;
}

ASTAssignArray::~ASTAssignArray(void){
    delete array;
    delete index;
    delete value;
}
ASTNode::Type ASTAssignArray::getType(void) const{
    return Type::AssignArray;
}

ASTFunction::~ASTFunction(void){
    delete body;
}
ASTNode::Type ASTFunction::getType(void) const{
    return Type::Function;
}

ASTCallFunction::~ASTCallFunction(void){
    for(ASTNode *argument : arguments)
        delete argument;
}
ASTNode::Type ASTCallFunction::getType(void) const{
    return Type::CallFunction;
}

ASTPrint::~ASTPrint(void){
    for(ASTNode *argument : arguments)
        delete argument;
}
ASTNode::Type ASTPrint::getType(void) const{
    return Type::Print;
}

ASTBlock::~ASTBlock(void){
    for(ASTNode *child : children)
        delete child;
}
ASTNode::Type ASTBlock::getType(void) const{
    return Type::Block;
}

ASTTop::~ASTTop(void){
    for(ASTNode *child : children)
        delete child;
}
ASTNode::Type ASTTop::getType(void) const{
    return Type::Top;
}

ASTLoop::~ASTLoop(void){
    delete condition;
    delete body;
}
ASTNode::Type ASTLoop::getType(void) const{
    return Type::Loop;
}

ASTConditional::~ASTConditional(void){
    delete condition;
    delete consequent;
    delete alternative;
}
ASTNode::Type ASTConditional::getType(void) const{
    return Type::Conditional;
}

ASTObject::~ASTObject(void){
    delete extends;
    for(ASTNode *member : members)
        delete member;
}
ASTNode::Type ASTObject::getType(void) const{
    return Type::Object;
}

ASTAssignField::~ASTAssignField(void){
    delete object;
    delete value;
}
ASTNode::Type ASTAssignField::getType(void) const{
    return Type::AssignField;
}

ASTAccessField::~ASTAccessField(void){
    delete object;
}
ASTNode::Type ASTAccessField::getType(void) const{
    return Type::AccessField;
}

ASTCallMethod::~ASTCallMethod(void){
    delete object;
    for(ASTNode *argument : arguments)
        delete argument;
}
ASTNode::Type ASTCallMethod::getType(void) const{
    return Type::CallMethod;
}
