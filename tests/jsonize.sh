#!/bin/bash

set -e

for i in *.fml; do
    echo "JSONizing $i"
    "$1" parse "$i" --format=json -o "$i".json
done
