#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include <exception>
#include <stdexcept>

class MyException: public std::runtime_error{
public:
    using runtime_error::runtime_error;
};

class ParsingError: public MyException{ // parsing in sense of parsing JSON, not FML
public:
    using MyException::MyException;
};
class SemanticError: public MyException{
public:
    using MyException::MyException;
};

#endif
