#include "interpreter.hpp"

#include <map>
#include <string>
#include <stdexcept>
#include <iostream>

#include "ast.hpp"
#include "util.hpp"
#include "exception.hpp"

void Object::take(void){
    refcount++;
}
void Object::release(void){
    refcount--;
    if(refcount == 0)
        delete this;
}



Frame::~Frame(void){
    for(auto it : mapping_var)
        it.second->release();
    for(auto it : mapping_fun)
        it.second->release();
}
bool Frame::getMappingVarIt(map_it_var& output, const std::string& name){
    map_it_var it = mapping_var.find(name);
    if(it != mapping_var.end()){
        output = it;
        return true;
    }
    if(parent)
        return parent->getMappingVarIt(output, name);
    return false;
}
bool Frame::getMappingFunIt(map_it_fun& output, const std::string& name){
    map_it_fun it = mapping_fun.find(name);
    if(it != mapping_fun.end()){
        output = it;
        return true;
    }
    if(parent)
        return parent->getMappingFunIt(output, name);
    return false;
}
Object *Frame::getMappingVar(const std::string& name){
    map_it_var it;
    if(!getMappingVarIt(it, name))
        return nullptr;
    return it->second;
}
ObjectFunction *Frame::getMappingFun(const std::string& name){
    map_it_fun it;
    if(!getMappingFunIt(it, name))
        return nullptr;
    return it->second;
}
void Frame::setMappingVar(const std::string& name, Object& obj){
    map_it_var it;
    if(!getMappingVarIt(it, name))
        mapping_var.emplace(name, &obj);
    else{
        it->second->release();
        it->second = &obj;
    }
    obj.take();
}
void Frame::setMappingVarDirect(const std::string& name, Object& obj){
    map_it_var it = mapping_var.find(name);
    if(it == mapping_var.end())
        mapping_var.emplace(name, &obj);
    else{
        it->second->release();
        it->second = &obj;
    }
    obj.take();
}
bool Frame::setMappingVarUnique(const std::string& name, Object& obj){
    map_it_var it = mapping_var.find(name);
    if(it != mapping_var.end())
        return false;
    mapping_var.emplace(name, &obj);
    obj.take();
    return true;
}
bool Frame::setMappingVarExisting(const std::string& name, Object& obj){
    map_it_var it = mapping_var.find(name);
    if(it == mapping_var.end())
        return false;
    it->second->release();
    it->second = &obj;
    obj.take();
    return true;
}
bool Frame::setMappingFun(const std::string& name, ObjectFunction& obj){
    map_it_fun it = mapping_fun.find(name);
    if(it != mapping_fun.end())
        return false;
    mapping_fun.emplace(name, &obj);
    obj.take();
    return true;
}



Object::Type ObjectInteger::getType(void) const{
    return Type::Integer;
}
Object::Type ObjectBoolean::getType(void) const{
    return Type::Boolean;
}
Object::Type ObjectNull::getType(void) const{
    return Type::Null;
}
ObjectNull singleton_null; // will always have refcount > 0 unless bug
ObjectNull *ObjectNull::singleton(void){
    singleton_null.take();
    return &singleton_null;
}
ObjectArray::~ObjectArray(void){
    for(int32_t i = 0; i < size; i++){
        if(data[i])
            data[i]->release();
    }
    delete[] data;
}
Object::Type ObjectArray::getType(void) const{
    return Type::Array;
}
Object::Type ObjectFunction::getType(void) const{
    return Type::Function;
}
ObjectObject::~ObjectObject(void){
    parent->release();
}
Object::Type ObjectObject::getType(void) const{
    return Type::Object;
}
Object& ObjectObject::getRootAncestor(void){
    Object *curr = this;
    while(curr->getType() == Object::Type::Object &&
          static_cast<ObjectObject *>(curr)->parent){
        curr = static_cast<ObjectObject *>(curr)->parent;
    }
    return *curr;
}
void ObjectObject::processFrame(void){
    if(parent->getType() == Object::Type::Object){
        ObjectObject *real_parent = static_cast<ObjectObject *>(parent);
        members.parent = &real_parent->members;
    }
}



Object *Interpret::runDispatch(const ASTNode& node, Frame& curr_frame){
    switch(node.getType()){
        case ASTNode::Type::Integer:
            return runInteger(static_cast<const ASTInteger&>(node));
        case ASTNode::Type::Boolean:
            return runBoolean(static_cast<const ASTBoolean&>(node));
        case ASTNode::Type::Null:
            return ObjectNull::singleton();
        case ASTNode::Type::Variable:
            return runVariable(static_cast<const ASTVariable&>(node), curr_frame);
        case ASTNode::Type::AccessVariable:
            return runAccessVariable(static_cast<const ASTAccessVariable&>(node), curr_frame);
        case ASTNode::Type::AssignVariable:
            return runAssignVariable(static_cast<const ASTAssignVariable&>(node), curr_frame);
        case ASTNode::Type::Array:
            return runArray(static_cast<const ASTArray&>(node), curr_frame);
        case ASTNode::Type::AccessArray:
            return runAccessArray(static_cast<const ASTAccessArray&>(node), curr_frame);
        case ASTNode::Type::AssignArray:
            return runAssignArray(static_cast<const ASTAssignArray&>(node), curr_frame);
        case ASTNode::Type::Function:
            return runFunction(static_cast<const ASTFunction&>(node), curr_frame);
        case ASTNode::Type::CallFunction:
            return runCallFunction(static_cast<const ASTCallFunction&>(node), curr_frame);
        case ASTNode::Type::Print:
            return runPrint(static_cast<const ASTPrint&>(node), curr_frame);
        case ASTNode::Type::Block:
            return runBlock(static_cast<const ASTBlock&>(node), curr_frame);
        case ASTNode::Type::Loop:
            return runLoop(static_cast<const ASTLoop&>(node), curr_frame);
        case ASTNode::Type::Conditional:
            return runConditional(static_cast<const ASTConditional&>(node), curr_frame);
        case ASTNode::Type::Object:
            return runObject(static_cast<const ASTObject&>(node), curr_frame);
        case ASTNode::Type::AssignField:
            return runAssignField(static_cast<const ASTAssignField&>(node), curr_frame);
        case ASTNode::Type::AccessField:
            return runAccessField(static_cast<const ASTAccessField&>(node), curr_frame);
        case ASTNode::Type::CallMethod:
            return runCallMethod(static_cast<const ASTCallMethod&>(node), curr_frame);
        default:
            MY_ASSERT(false); // cannot happen
    }
}
void Interpret::runTop(const ASTTop& top){
    Frame top_frame;
    for(const ASTNode *n : top.children)
        runDispatch(*n, top_frame)->release();
}
Object *Interpret::runInteger(const ASTInteger& integer){
    return new ObjectInteger(integer.value);
}
Object *Interpret::runBoolean(const ASTBoolean& boolean){
    return new ObjectBoolean(boolean.value);
}
Object *Interpret::runVariable(const ASTVariable& variable, Frame& curr_frame){
    Object *value = runDispatch(*variable.value, curr_frame);
    curr_frame.setMappingVarDirect(variable.name, *value);
    return value;
}
Object *Interpret::runAccessVariable(const ASTAccessVariable& access_variable, Frame& curr_frame){
    Object *obj = curr_frame.getMappingVar(access_variable.name);
    if(!obj)
        throw SemanticError("undeclared variable");
    obj->take();
    return obj;
}
Object *Interpret::runAssignVariable(const ASTAssignVariable& assign_variable, Frame& curr_frame){
    Object *value = runDispatch(*assign_variable.value, curr_frame);
    curr_frame.setMappingVar(assign_variable.name, *value);
    return value;
}
Object *Interpret::runArray(const ASTArray& array, Frame& curr_frame){
    Object *size = runDispatch(*array.size, curr_frame);
    if(size->getType() != Object::Type::Integer)
        throw SemanticError("Array size isn't an integer");
    int32_t real_size = static_cast<ObjectInteger *>(size)->value;
    size->release();
    ObjectArray *result = new ObjectArray(real_size);
    for(int32_t i = 0; i < real_size; i++){
        Frame value_frame(curr_frame);
        result->data[i] = runDispatch(*array.value, value_frame);
    }
    return result;
}
Object *Interpret::runAccessArray(const ASTAccessArray& access_array, Frame& curr_frame){
    Object *array = runDispatch(*access_array.array, curr_frame);
    if(array->getType() != Object::Type::Array)
        throw SemanticError("Attempting to index something not an array");
    ObjectArray *real_array = static_cast<ObjectArray *>(array);
    Object *index = runDispatch(*access_array.index, curr_frame);
    if(index->getType() != Object::Type::Integer)
        throw SemanticError("Array index must be an integer");
    int32_t real_index = static_cast<ObjectInteger *>(index)->value;
    index->release();
    if(real_index >= real_array->size)
        throw SemanticError("Attempting to index an array beyond its size");
    Object *result = real_array->data[real_index];
    array->release();
    result->take();
    return result;
}
Object *Interpret::runAssignArray(const ASTAssignArray& assign_array, Frame& curr_frame){
    Object *array = runDispatch(*assign_array.array, curr_frame);
    if(array->getType() != Object::Type::Array)
        throw SemanticError("Attempting to index something not an array");
    ObjectArray *real_array = static_cast<ObjectArray *>(array);
    Object *index = runDispatch(*assign_array.index, curr_frame);
    if(index->getType() != Object::Type::Integer)
        throw SemanticError("Array index must be an integer");
    int32_t real_index = static_cast<ObjectInteger *>(index)->value;
    index->release();
    if(real_index >= real_array->size)
        throw SemanticError("Attempting to index an array beyond its size");
    Object *value = runDispatch(*assign_array.value, curr_frame);
    real_array->data[real_index]->release();
    real_array->data[real_index] = value;
    array->release();
    value->take();
    return value;
}
Object *Interpret::runFunction(const ASTFunction& function, Frame& curr_frame){
    ObjectFunction *value = new ObjectFunction(function);
    if(!curr_frame.setMappingFun(function.name, *value))
        throw SemanticError("Attempting to redefine a function");
    value->release();
    return ObjectNull::singleton();
}
Object *Interpret::runCallFunction(const ASTCallFunction& call_function, Frame& curr_frame){
    ObjectFunction *obj = curr_frame.getMappingFun(call_function.name);
    if(!obj)
        throw SemanticError("Function not found");
    std::vector<Object *> arguments;
    for(const ASTNode *node : call_function.arguments)
        arguments.push_back(runDispatch(*node, curr_frame));
    Frame function_frame(curr_frame);
    for(size_t i = 0; i < call_function.arguments.size(); i++){
        function_frame.setMappingVarDirect(obj->function.parameters[i], *arguments[i]);
        arguments[i]->release(); // they are now taken within function frame
    }
    return runDispatch(*obj->function.body, function_frame);
}
void Interpret::printObject(const Object& obj){
    switch(obj.getType()){
        case Object::Type::Integer:
            std::cout << static_cast<const ObjectInteger&>(obj).value;
            break;
        case Object::Type::Boolean:
            std::cout << (static_cast<const ObjectBoolean&>(obj).value ? "true" : "false");
            break;
        case Object::Type::Null:
            std::cout << "null";
            break;
        case Object::Type::Array:{
            const ObjectArray& array = static_cast<const ObjectArray&>(obj);
            std::cout << "[";
            for(int32_t i = 0; i < array.size; i++){
                printObject(*array.data[i]);
                if(i + 1 != array.size)
                    std::cout << ",";
            }
            std::cout << "]";
            break;
        }
        default:
            throw SemanticError("Unsupported print type");
    }
}
#define BACKLASH_ITEM(input, output) \
    case input: \
    std::cout << output; \
    break
Object *Interpret::runPrint(const ASTPrint& print, Frame& curr_frame){
    std::vector<Object *> arguments;
    for(const ASTNode *node : print.arguments)
        arguments.push_back(runDispatch(*node, curr_frame));
    const char *c_str = print.format.c_str();
    size_t i = 0;
    size_t curr_arg = 0;
    bool backlash_mode = false;
    for(char curr_char; (curr_char = c_str[i]) != '\0'; i++){
        if(backlash_mode){
            switch(curr_char){
                BACKLASH_ITEM('~', "~");
                BACKLASH_ITEM('n', std::endl);
                BACKLASH_ITEM('"', "\"");
                BACKLASH_ITEM('t', "\t");
                BACKLASH_ITEM('\\', "\\");
                case 'r':
                    break; // doesnt work in C++
                default:
                    throw SemanticError("Unknown \\ operator");
            }
            backlash_mode = false;
        }else{
            if(curr_char == '\\')
                backlash_mode = true;
            else if(curr_char == '~'){
                printObject(*arguments[curr_arg]);
                curr_arg++;
            }else
                std::cout << curr_char;
        }
    }
    if(backlash_mode)
        throw SemanticError("Print cannot end with \\");
    for(auto node : arguments)
        node->release();
    return ObjectNull::singleton();
}
Object *Interpret::runBlock(const ASTBlock& block, Frame& curr_frame){
    Frame block_frame(curr_frame);
    std::vector<Object *> children;
    for(const ASTNode *node : block.children)
        children.push_back(runDispatch(*node, block_frame));
    if(children.size() == 0)
        return ObjectNull::singleton();
    else{
        for(size_t i = 0; i < children.size() - 1; i++) // release all except last one
            children[i]->release();
        return children.back();
    }
}
bool Interpret::evaluatesTrue(const Object& input){
    if(input.getType() == Object::Type::Boolean){
        const ObjectBoolean& real_input = static_cast<const ObjectBoolean&>(input);
        return real_input.value;
    }
    if(input.getType() == Object::Type::Null)
        return false;
    return true;
}
Object *Interpret::runLoop(const ASTLoop& loop, Frame& curr_frame){
    while(true){
        Object *cond_val = runDispatch(*loop.condition, curr_frame);
        bool t = evaluatesTrue(*cond_val);
        cond_val->release();
        if(!t)
            return ObjectNull::singleton();
        runDispatch(*loop.body, curr_frame)->release();
    }
}
Object *Interpret::runConditional(const ASTConditional& conditional, Frame& curr_frame){
    Object *cond_val = runDispatch(*conditional.condition, curr_frame);
    bool t = evaluatesTrue(*cond_val);
    cond_val->release();
    if(t)
        return runDispatch(*conditional.consequent, curr_frame);
    else
        return runDispatch(*conditional.alternative, curr_frame);
}
Object *Interpret::runObject(const ASTObject& object, Frame& curr_frame){
    Object *extends = runDispatch(*object.extends, curr_frame);
    if(extends->getType() == Object::Type::Function)
        throw SemanticError("Cannot extend a function");
    ObjectObject *result = new ObjectObject(*extends);
    extends->release();
    std::vector<Object *> members;
    for(const ASTNode *node : object.members){
        if(node->getType() == ASTNode::Type::Variable){
            const ASTVariable& variable = static_cast<const ASTVariable&>(*node);
            Object *value = runDispatch(*variable.value, curr_frame);
            if(!result->members.setMappingVarUnique(variable.name, *value))
                throw SemanticError("Attempting to redefine a field");
            value->release();
        }else if(node->getType() == ASTNode::Type::Function){
            const ASTFunction& function = static_cast<const ASTFunction&>(*node);
            ObjectFunction *value = new ObjectFunction(function);
            if(!result->members.setMappingFun(function.name, *value))
                throw SemanticError("Attempting to redefine a method");
            value->release();
        }else
            throw SemanticError("Method member must be variable or function");
    }
    return result;
}
Object *Interpret::runAssignField(const ASTAssignField& assign_field, Frame& curr_frame){
    Object *obj = runDispatch(*assign_field.object, curr_frame);
    if(obj->getType() != Object::Type::Object)
        throw SemanticError("Attempted to assign to a field of something not an object");
    ObjectObject *real_obj = static_cast<ObjectObject *>(obj);
    Object *value = runDispatch(*assign_field.value, curr_frame);
    if(!real_obj->members.setMappingVarExisting(assign_field.field, *value)) // TODO: can we assign a new variable into a method field
        throw SemanticError("Attempting to assign to a non-existent field");
    obj->release();
    return value;
}
Object *Interpret::runAccessField(const ASTAccessField& access_field, Frame& curr_frame){
    Object *obj = runDispatch(*access_field.object, curr_frame);
    if(obj->getType() != Object::Type::Object)
        throw SemanticError("Attempted to access a field of something not an object");
    ObjectObject *real_obj = static_cast<ObjectObject *>(obj);
    Object *result = real_obj->members.getMappingVar(access_field.field);
    obj->release();
    if(!result)
        throw SemanticError("Field not found");
    result->take();
    return result;
}
Object *Interpret::runCallMethod(const ASTCallMethod& call_method, Frame& curr_frame){
    Object *obj = runDispatch(*call_method.object, curr_frame);
    Object *result;
    if(obj->getType() == Object::Type::Object)
        result = callRealMethod(static_cast<ObjectObject *>(obj), call_method, curr_frame);
    else{
        result = callBuiltinMethod(obj, call_method, curr_frame);
        if(!result)
            throw SemanticError("Attempted to call a builtin method with incorrect signature");
    }
    obj->release();
    return result;
}
Object *Interpret::callBooleanMethod(ObjectBoolean *a, const ASTCallMethod& call_method, Frame& curr_frame){
    bool va = a->value;
    if(call_method.arguments.size() != 1)
        return nullptr;
    Object *tmp_b = runDispatch(*call_method.arguments[0], curr_frame);
    if(tmp_b->getType() != Object::Type::Boolean)
        return nullptr;
    bool vb = static_cast<ObjectBoolean *>(tmp_b)->value;
    tmp_b->release();
    if(call_method.name == "|")
        return new ObjectInteger(va || vb);
    else if(call_method.name == "&")
        return new ObjectInteger(va && vb);
    else if(call_method.name == "==")
        return new ObjectBoolean(va == vb);
    else if(call_method.name == "!=")
        return new ObjectBoolean(va != vb);
    else
        return nullptr;
}
Object *Interpret::callIntegerMethod(ObjectInteger *a, const ASTCallMethod& call_method, Frame& curr_frame){
    int32_t va = a->value;
    if(call_method.arguments.size() != 1)
        return nullptr;
    Object *tmp_b = runDispatch(*call_method.arguments[0], curr_frame);
    if(tmp_b->getType() != Object::Type::Integer)
        return nullptr;
    int32_t vb = static_cast<ObjectInteger *>(tmp_b)->value;
    tmp_b->release();
    if(call_method.name == "+")
        return new ObjectInteger(va + vb);
    else if(call_method.name == "-")
        return new ObjectInteger(va - vb);
    else if(call_method.name == "*")
        return new ObjectInteger(va * vb);
    else if(call_method.name == "/")
        return new ObjectInteger(va / vb);
    else if(call_method.name == "%")
        return new ObjectInteger(va % vb);
    else if(call_method.name == "==")
        return new ObjectBoolean(va == vb);
    else if(call_method.name == "!=")
        return new ObjectBoolean(va != vb);
    else if(call_method.name == "<")
        return new ObjectBoolean(va < vb);
    else if(call_method.name == ">")
        return new ObjectBoolean(va > vb);
    else if(call_method.name == "<=")
        return new ObjectBoolean(va <= vb);
    else if(call_method.name == ">=")
        return new ObjectBoolean(va >= vb);
    else
        return nullptr;
}
Object *Interpret::callBuiltinMethod(Object *obj, const ASTCallMethod& call_method, Frame& curr_frame){
    if(obj->getType() == Object::Type::Integer)
        return callIntegerMethod(static_cast<ObjectInteger *>(obj), call_method, curr_frame);
    else if(obj->getType() == Object::Type::Boolean)
        return callBooleanMethod(static_cast<ObjectBoolean *>(obj), call_method, curr_frame);
    else
        throw SemanticError("Builtin methods for this type are not implemented");
}
Object *Interpret::callRealMethod(ObjectObject *obj, const ASTCallMethod& call_method, Frame& curr_frame){
    ObjectFunction *method = obj->members.getMappingFun(call_method.name);
    if(!method){
        Object& root_ancestor = obj->getRootAncestor();
        Object *result = callBuiltinMethod(&root_ancestor, call_method, curr_frame);
        if(!result)
            throw SemanticError("Attempted to call undefined object method, or a builtin method with incorrect signature");
        return result;
    }else{
        std::vector<Object *> arguments;
        for(const ASTNode *node : call_method.arguments)
            arguments.push_back(runDispatch(*node, curr_frame));
        Frame method_frame(curr_frame);
        method_frame.setMappingVarDirect("this", *obj);
        for(size_t i = 0; i < call_method.arguments.size(); i++){
            method_frame.setMappingVarDirect(method->function.parameters[i], *arguments[i]);
            arguments[i]->release(); // they are now taken within method frame
        }
        return runDispatch(*method->function.body, method_frame);
    }
}
