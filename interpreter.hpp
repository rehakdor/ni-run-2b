#ifndef INTERPRETER_HPP
#define INTERPRETER_HPP

#include <map>
#include <string>
#include <cstring>

#include "ast.hpp"

class Object{
protected:
    unsigned int refcount;

public:
    enum class Type{
        Integer,
        Boolean,
        Null,
        Array,
        Function,
        Object
    };

    Object(void)
    : refcount(1){
    }
    virtual ~Object(void) = default;
    virtual Type getType(void) const = 0;

    void take(void);
    void release(void);
};

class ObjectFunction;

class Frame{
public:
    Frame *parent;
    std::map<std::string, Object *> mapping_var;
    std::map<std::string, ObjectFunction *> mapping_fun;

    Frame(void)
    : parent(nullptr){
    }
    Frame(Frame& pparent)
    : parent(&pparent){
    }
    ~Frame(void);

    using map_it_var = typename std::map<std::string, Object *>::iterator;
    using map_it_fun = typename std::map<std::string, ObjectFunction *>::iterator;
    bool getMappingVarIt(map_it_var& output, const std::string& name);
    bool getMappingFunIt(map_it_fun& output, const std::string& name);

    Object *getMappingVar(const std::string& name);
    ObjectFunction *getMappingFun(const std::string& name);
    void setMappingVar(const std::string& name, Object& obj); // variable assignment
    void setMappingVarDirect(const std::string& name, Object& obj); // variable definition
    bool setMappingVarUnique(const std::string& name, Object& obj); // field definition
    bool setMappingVarExisting(const std::string& name, Object& obj); // field assignment
    bool setMappingFun(const std::string& name, ObjectFunction& obj); // function definition, method definition
};

class ObjectInteger: public Object{
public:
    int32_t value;

    ObjectInteger(int32_t pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ObjectBoolean: public Object{
public:
    bool value;

    ObjectBoolean(bool pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ObjectNull: public Object{
public:
    Type getType(void) const override;

    static ObjectNull *singleton(void);
};
class ObjectArray: public Object{
public:
    int32_t size;
    Object **data;

    ObjectArray(int32_t psize)
    : size(psize){
        data = new Object *[psize];
        std::memset(data, 0, psize * sizeof(Object *));
    }
    ~ObjectArray(void);
    Type getType(void) const override;
};
class ObjectFunction: public Object{
public:
    const ASTFunction& function;

    ObjectFunction(const ASTFunction& pfunction)
    : function(pfunction){
    }
    Type getType(void) const override;
};
class ObjectObject: public Object{
public:
    Object *parent;
    Frame members;

    ObjectObject(Object& pparent)
    : parent(&pparent){
        parent->take();
        processFrame();
    }
    ~ObjectObject(void);

    Type getType(void) const override;
    Object& getRootAncestor(void);

protected:
    void processFrame(void);
};

class Interpret{
public:
    void runTop(const ASTTop& top);

protected:
    Object *runDispatch(const ASTNode& node, Frame& curr_frame);
    Object *runInteger(const ASTInteger& integer);
    Object *runBoolean(const ASTBoolean& boolean);
    Object *runVariable(const ASTVariable& variable, Frame& curr_frame);
    Object *runAccessVariable(const ASTAccessVariable& access_variable, Frame& curr_frame);
    Object *runAssignVariable(const ASTAssignVariable& assign_variable, Frame& curr_frame);
    Object *runArray(const ASTArray& array, Frame& curr_frame);
    Object *runAccessArray(const ASTAccessArray& access_array, Frame& curr_frame);
    Object *runAssignArray(const ASTAssignArray& assign_array, Frame& curr_frame);
    Object *runFunction(const ASTFunction& function, Frame& curr_frame);
    Object *runCallFunction(const ASTCallFunction& call_function, Frame& curr_frame);
    void printObject(const Object& obj);
    Object *runPrint(const ASTPrint& print, Frame& curr_frame);
    Object *runBlock(const ASTBlock& block, Frame& curr_frame);
    bool evaluatesTrue(const Object& input);
    Object *runLoop(const ASTLoop& loop, Frame& curr_frame);
    Object *runConditional(const ASTConditional& conditional, Frame& curr_frame);
    Object *runObject(const ASTObject& object, Frame& curr_frame);
    Object *runAssignField(const ASTAssignField& assign_field, Frame& curr_frame);
    Object *runAccessField(const ASTAccessField& access_field, Frame& curr_frame);
    Object *runCallMethod(const ASTCallMethod& call_method, Frame& curr_frame);
    Object *callBooleanMethod(ObjectBoolean *a, const ASTCallMethod& call_method, Frame& curr_frame);
    Object *callIntegerMethod(ObjectInteger *a, const ASTCallMethod& call_method, Frame& curr_frame);
    Object *callBuiltinMethod(Object *obj, const ASTCallMethod& call_method, Frame& curr_frame);
    Object *callRealMethod(ObjectObject *obj, const ASTCallMethod& call_method, Frame& curr_frame);
};

#endif
