#include "convertor.hpp"

#include <fstream>
#include <exception>

#include <jsoncpp/json/json.h>

#include "ast.hpp"
#include "exception.hpp"

ASTNode *ConvertorJSON::convert(std::ifstream& stream){
    Json::Value json;
    JSONCPP_STRING err;
    Json::CharReaderBuilder builder;
    if(!parseFromStream(builder, stream, &json, &err))
        throw ParsingError("failed parsing JSON");
    return convertDispatch(json);
}
ASTNode *ConvertorJSON::convertDispatch(const Json::Value& json){
    if(json.isObject() && json.size() == 1){
        // Ideally, this would be a hashmap for performance
        if(json.isMember("Top"))
            return convertTop(json["Top"]);
        if(json.isMember("Integer"))
            return convertInteger(json["Integer"]);
        if(json.isMember("Boolean"))
            return convertBoolean(json["Boolean"]);
        if(json.isMember("Variable"))
            return convertVariable(json["Variable"]);
        if(json.isMember("AccessVariable"))
            return convertAccessVariable(json["AccessVariable"]);
        if(json.isMember("AssignVariable"))
            return convertAssignVariable(json["AssignVariable"]);
        if(json.isMember("Array"))
            return convertArray(json["Array"]);
        if(json.isMember("AccessArray"))
            return convertAccessArray(json["AccessArray"]);
        if(json.isMember("AssignArray"))
            return convertAssignArray(json["AssignArray"]);
        if(json.isMember("Function"))
            return convertFunction(json["Function"]);
        if(json.isMember("CallFunction"))
            return convertCallFunction(json["CallFunction"]);
        if(json.isMember("Print"))
            return convertPrint(json["Print"]);
        if(json.isMember("Block"))
            return convertBlock(json["Block"]);
        if(json.isMember("Loop"))
            return convertLoop(json["Loop"]);
        if(json.isMember("Conditional"))
            return convertConditional(json["Conditional"]);
        if(json.isMember("Object"))
            return convertObject(json["Object"]);
        if(json.isMember("AssignField"))
            return convertAssignField(json["AssignField"]);
        if(json.isMember("AccessField"))
            return convertAccessField(json["AccessField"]);
        if(json.isMember("CallMethod"))
            return convertCallMethod(json["CallMethod"]);
    }else if(json.isString() && json.asString() == "Null")
        return convertNull();
    throw ParsingError("failed convertDispatch");
}
ASTInteger *ConvertorJSON::convertInteger(const Json::Value& json){
    if(json.isUInt())
        return new ASTInteger(json.asUInt());
    else if(json.isInt())
        return new ASTInteger(json.asInt());
    else
        throw ParsingError("failed convertInteger");
}
ASTBoolean *ConvertorJSON::convertBoolean(const Json::Value& json){
    if(!json.isBool())
        throw ParsingError("failed convertBool");
    return new ASTBoolean(json.asBool());
}
ASTNull *ConvertorJSON::convertNull(void){
    return new ASTNull;
}
ASTVariable *ConvertorJSON::convertVariable(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertVariable");
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertVariable (name)");
    Json::Value jvalue = json["value"];
    return new ASTVariable(jname.asString(), convertDispatch(jvalue));
}
ASTAccessVariable *ConvertorJSON::convertAccessVariable(const Json::Value& json){
    if(!json.isObject() || json.size() != 1)
        throw ParsingError("failed convertAccessVariable");
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertAccessVariable (name)");
    return new ASTAccessVariable(jname.asString());
}
ASTAssignVariable *ConvertorJSON::convertAssignVariable(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertAssignVariable");
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertAssignVariable (name)");
    Json::Value jvalue = json["value"];
    return new ASTAssignVariable(jname.asString(), convertDispatch(jvalue));
}
ASTArray *ConvertorJSON::convertArray(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertArray");
    Json::Value jsize = json["size"];
    Json::Value jvalue = json["value"];
    return new ASTArray(convertDispatch(jsize), convertDispatch(jvalue));
}
ASTAccessArray *ConvertorJSON::convertAccessArray(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertAccessArray");
    Json::Value jarray = json["array"];
    Json::Value jindex = json["index"];
    return new ASTAccessArray(convertDispatch(jarray), convertDispatch(jindex));
}
ASTAssignArray *ConvertorJSON::convertAssignArray(const Json::Value& json){
    if(!json.isObject() || json.size() != 3)
        throw ParsingError("failed convertAssignArray");
    Json::Value jarray = json["array"];
    Json::Value jindex = json["index"];
    Json::Value jvalue = json["value"];
    return new ASTAssignArray(convertDispatch(jarray), convertDispatch(jindex), convertDispatch(jvalue));
}
ASTFunction *ConvertorJSON::convertFunction(const Json::Value& json){
    if(!json.isObject() || json.size() != 3)
        throw ParsingError("failed convertFunction");
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertFunction (name)");
    Json::Value jparameters = json["parameters"];
    if(!jparameters.isArray())
        throw ParsingError("failed convertFunction (parameters)");
    std::vector<std::string> parameters;
    for(Json::ArrayIndex i = 0; i < jparameters.size(); i++){
        Json::Value jparameter = jparameters[i];
        if(!jparameter.isString())
            throw ParsingError("failed convertFunction (parameter)");
        parameters.push_back(jparameter.asString());
    }
    Json::Value jbody = json["body"];
    return new ASTFunction(jname.asString(), parameters, convertDispatch(jbody));
}
ASTCallFunction *ConvertorJSON::convertCallFunction(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertCallFunction");
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertCallFunction (name)");
    Json::Value jarguments = json["arguments"];
    if(!jarguments.isArray())
        throw ParsingError("failed convertCallFunction (arguments)");
    std::vector<ASTNode *> arguments;
    for(Json::ArrayIndex i = 0; i < jarguments.size(); i++)
        arguments.push_back(convertDispatch(jarguments[i]));
    return new ASTCallFunction(jname.asString(), arguments);
}
ASTPrint *ConvertorJSON::convertPrint(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertPrint");
    Json::Value jformat = json["format"];
    if(!jformat.isString())
        throw ParsingError("failed convertPrint (format)");
    Json::Value jarguments = json["arguments"];
    if(!jarguments.isArray())
        throw ParsingError("failed convertPrint (arguments)");
    std::vector<ASTNode *> arguments;
    for(Json::ArrayIndex i = 0; i < jarguments.size(); i++)
        arguments.push_back(convertDispatch(jarguments[i]));
    return new ASTPrint(jformat.asString(), arguments);
}
ASTBlock *ConvertorJSON::convertBlock(const Json::Value& json){
    if(!json.isArray())
        throw ParsingError("failed convertBlock");
    std::vector<ASTNode *> children;
    for(Json::ArrayIndex i = 0; i < json.size(); i++)
        children.push_back(convertDispatch(json[i]));
    return new ASTBlock(children);
}
ASTTop *ConvertorJSON::convertTop(const Json::Value& json){
    if(!json.isArray())
        throw ParsingError("failed convertTop");
    std::vector<ASTNode *> children;
    for(Json::ArrayIndex i = 0; i < json.size(); i++)
        children.push_back(convertDispatch(json[i]));
    return new ASTTop(children);
}
ASTLoop *ConvertorJSON::convertLoop(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertLoop");
    Json::Value jcondition = json["condition"];
    Json::Value jbody = json["body"];
    return new ASTLoop(convertDispatch(jcondition), convertDispatch(jbody));
}
ASTConditional *ConvertorJSON::convertConditional(const Json::Value& json){
    if(!json.isObject() || json.size() != 3)
        throw ParsingError("failed convertConditional");
    Json::Value jcondition = json["condition"];
    Json::Value jconsequent = json["consequent"];
    Json::Value jalternative = json["alternative"];
    return new ASTConditional(convertDispatch(jcondition), convertDispatch(jconsequent), convertDispatch(jalternative));
}
ASTObject *ConvertorJSON::convertObject(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertObject");
    Json::Value jextends = json["extends"];
    Json::Value jmembers = json["members"];
    if(!jmembers.isArray())
        throw ParsingError("failed convertObject (members)");
    std::vector<ASTNode *> members;
    for(Json::ArrayIndex i = 0; i < jmembers.size(); i++){
        ASTNode *member = convertDispatch(jmembers[i]);
        if(member->getType() != ASTNode::Type::Variable &&
           member->getType() != ASTNode::Type::Function)
           throw ParsingError("failed convertObject (member isn't variable or function)");
        members.push_back(member);
    }
    return new ASTObject(convertDispatch(jextends), members);
}
ASTAssignField *ConvertorJSON::convertAssignField(const Json::Value& json){
    if(!json.isObject() || json.size() != 3)
        throw ParsingError("failed convertAssignField");
    Json::Value jobject = json["object"];
    Json::Value jfield = json["field"];
    if(!jfield.isString())
        throw ParsingError("failed convertAccessField (field)");
    Json::Value jvalue = json["value"];
    return new ASTAssignField(convertDispatch(jobject), jfield.asString(), convertDispatch(jvalue));
}
ASTAccessField *ConvertorJSON::convertAccessField(const Json::Value& json){
    if(!json.isObject() || json.size() != 2)
        throw ParsingError("failed convertAccessField");
    Json::Value jobject = json["object"];
    Json::Value jfield = json["field"];
    if(!jfield.isString())
        throw ParsingError("failed convertAccessField (field)");
    return new ASTAccessField(convertDispatch(jobject), jfield.asString());
}
ASTCallMethod *ConvertorJSON::convertCallMethod(const Json::Value& json){
    if(!json.isObject() || json.size() != 3)
        throw ParsingError("failed convertCallMethod");
    Json::Value jobject = json["object"];
    Json::Value jname = json["name"];
    if(!jname.isString())
        throw ParsingError("failed convertCallMethod (name)");
    Json::Value jarguments = json["arguments"];
    if(!jarguments.isArray())
        throw ParsingError("failed convertCallMethod (arguments)");
    std::vector<ASTNode *> arguments;
    for(Json::ArrayIndex i = 0; i < jarguments.size(); i++)
        arguments.push_back(convertDispatch(jarguments[i]));
    return new ASTCallMethod(convertDispatch(jobject), jname.asString(), arguments);
}
