.PHONY: all clean
.SUFFICES:

CXX = clang++
CXXFLAGS ?= -O3 -Wall -pedantic -g
CFLAGS_BASE := -std=c++11 -fno-strict-aliasing
LDFLAGS_BASE := -ljsoncpp

SRC := ast convertor interpreter main
OBJ := $(addsuffix .o,$(SRC))
EXE := main

TESTS := array block cond int object1 object2 print_var print_var2 fibonacci roman stack triangle
JSONS := $(addprefix tests/,$(addsuffix .fml.json,$(TESTS)))

all: $(EXE)
clean:
	-rm $(EXE) $(OBJ) $(JSONS)

ast.o: ast.hpp
convertor.o: convertor.hpp ast.hpp exception.hpp
interpreter.o: interpreter.hpp ast.hpp util.hpp exception.hpp
main.o: ast.hpp convertor.hpp interpreter.hpp
%.o: %.cpp Makefile
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_BASE) $< -o $@
$(EXE): $(OBJ)
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) $^ $(LDFLAGS_BASE) $(LDFLAGS) -o $@
